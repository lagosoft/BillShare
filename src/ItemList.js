import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { firebase } from './services/dataUtils';

class ItemList extends Component {

    constructor(props){
        super(props);
    }

    componentWillMount(){
        this.loadData();
    }

    async loadData() {
        firebase.database().ref('items')
            .on('value', (snapshot) => {
                console.log("GOT ", snapshot.val());
            });
    }

    render(){
        const {navigate} = this.props.navigation;
        const params = this.props.navigation.state.params;
        return(
            <View style={styles.list}>
                <TouchableOpacity onPress={() => navigate('ItemDetail', {itemId: 'SKU-0123'})}>
                    <Text style={styles.text}>{params.greeting || 'hi person!'}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    list: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'darkorange',
    },
    text: {
        fontSize: 27,
        color: '#FFF',
    },
});

export default ItemList;