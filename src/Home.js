import React, { Component } from 'react';
import { StyleSheet, StatusBar, View, Text, TouchableOpacity } from 'react-native';

class Home extends Component {
    render() {
        const {navigate} = this.props.navigation;

        return (
            <View style={styles.mainHome}>
                <TouchableOpacity onPress={() => navigate('ItemList', {greeting: 'Yo ladies and gentlemen!'} )}>
                    <Text style={styles.homeText}>THIS IS HOME</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainHome: {
        flex: 1,
        backgroundColor: 'yellow',
        justifyContent: 'center',
        alignItems: 'center',
    },
    homeText: {
        fontSize: 20,
    }
});

export default Home;