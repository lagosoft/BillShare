import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

class ItemDetail extends Component {
    render(){
        const {itemId} = this.props.navigation.state.params;
        console.log("ITEM ID", itemId);
        return(
            <View style={styles.detail}>
                <Text style={styles.detailText}>{`Item Details for item ${itemId}`}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    detail: {
        flex: 1,
        backgroundColor: 'rebeccapurple',
        justifyContent: 'center',
        alignItems: 'center',
    },
    detailText: {
        color: '#fff',
        fontSize: 17,
    },
});

export default ItemDetail;