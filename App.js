import React from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import Home from './src/Home';
import ItemList from './src/ItemList'
import ItemDetail from './src/ItemDetail'

export default class App extends React.Component {

  render() {

    const MainNavigator = StackNavigator({
      Home: { screen: Home },
      ItemList: { screen: ItemList },
      ItemDetail: { screen: ItemDetail },
    }, {
      headerMode: 'none',
    });

    return (
      <KeyboardAwareScrollView
          extraHeight={100}
          resetScrollToCoords={{ x: 0, y: 0 }}
          contentContainerStyle={[styles.mainContainer]}
          scrollEnabled={false}
      >
        <StatusBar barStyle='dark-content'/>
        <MainNavigator/>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
      flex: 1,
      backgroundColor: 'rebeccapurple',
      justifyContent: 'flex-end',
  }
});